VERSION=v2.3.5

build:
	go mod tidy && go build ./...

gomod:
	go get chainmaker.org/chainmaker/common/v2@v2.3.4
	go get chainmaker.org/chainmaker/localconf/v2@v2.3.4
	go get chainmaker.org/chainmaker/logger/v2@v2.3.4
	go get chainmaker.org/chainmaker/net-common@v1.2.5
	go get chainmaker.org/chainmaker/net-libp2p@v1.2.5
	go get chainmaker.org/chainmaker/net-liquid@v1.1.3
	go get chainmaker.org/chainmaker/pb-go/v2@$(VERSION)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION)
	go get chainmaker.org/chainmaker/utils/v2@$(VERSION)
	go mod tidy

lint:
	golangci-lint run ./...
